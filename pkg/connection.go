package pkg

import (
	"bufio"
	"net"
	"strconv"
	"strings"
	"time"

	"github.com/sirupsen/logrus"
)

/**
 * Various user states, recorded in the connection struct
 */
const (
	NOT_LOGGED int8 = 1 + iota
	STATE_WELCOME
	STATE_LOGIN_USERNAME
	STATE_LOGIN_PASSWORD
	STATE_LOGIN_MENU
	STATE_CHARACTER_CREATION
	STATE_PLAYING
)

const (
	MENU_START_GAME int8 = 1 + iota
	MENU_CHANGE_PASSWORD
	MENU_DELETE_CHARACTER
	MENU_EXIT = 0
)

/**
 * Connection-related constants
 */
const MAX_PASSWORD_FAILURES = 3

type Connection struct {
	conn             net.Conn
	timeConnected    time.Time
	state            int8
	username         string
	char             *Character
	passwordFailures int
}

/**
 * Writes raw bytes to the current connection
 */
func (connection *Connection) Write(message string) {
	if _, err := connection.conn.Write([]byte(message)); err != nil {
		logrus.Error(err)
		connection.char.Connection = nil
		connection.conn.Close()
	}
}

/**
 * Writes the MOTD (Message of the day) to the current connection
 */
func (connection *Connection) sendMOTD() {
	connection.Write(ServerInstance.Motd + "What is your name? ")
}

/**
 * Writes the Menu to the current connection
 */
func (connection *Connection) sendMenu() {
	connection.Write(ServerInstance.Menu)
}

/**
 * Loops current connection to treat input and output
 */
func (connection *Connection) listen() {
	reader := bufio.NewReader(connection.conn)

	connection.sendMOTD()
	connection.state = STATE_LOGIN_USERNAME

	for {
		message, err := reader.ReadString('\n')

		if err != nil {
			connection.conn.Close()
			ServerInstance.onClientConnectionClosed(connection, err)
			return
		}

		message = strings.TrimSpace(message)

		switch connection.state {
		// Player has just seen the MOTD, and is asked for username
		case STATE_LOGIN_USERNAME:
			connection.username = message
			if userExists(message) {
				connection.state = STATE_LOGIN_PASSWORD
				connection.Write("Your password? ")
			} else {
				connection.Write("This played does not exists!\n")
				connection.Write("What is your name? ")
			}

			// Player is being asked to authenticate
		case STATE_LOGIN_PASSWORD:
			exists, char := ServerInstance.login(connection.username, message)

			if exists {
				if char != nil {
					// auth succeeded, do a bit of housekeeping
					connection.char = char
					connection.char.Connection = connection
					//player.setConnection(connection)

					ServerInstance.onCharacterAuthenticated(connection)
				} else {
					// auth fails, try again
					connection.Write("Sorry, that wasn't right. Try again: ")

					connection.passwordFailures++
					if connection.passwordFailures > MAX_PASSWORD_FAILURES {
						connection.Write("Pfft...  Goodbye.")
						connection.conn.Close()
					}

				}
			} else {
				connection.state = STATE_CHARACTER_CREATION
			}

		case STATE_LOGIN_MENU:
			option, err := strconv.Atoi(message)

			if err != nil {
				connection.Write("Invalid option, try again.\n")
			}
			switch int8(option) {
			case MENU_START_GAME:
				connection.state = STATE_PLAYING
				connection.Write("The world darkens...\n")
				DAHWORUDO.roomList[connection.char.CurrentRoom.id].addCharacter(connection.char)
				connection.char.do("look", []string{})

			case MENU_CHANGE_PASSWORD:
			case MENU_DELETE_CHARACTER:
			case MENU_EXIT:
				connection.Write("Until next time!")
				connection.conn.Close()
			}

		default:
			ServerInstance.onMessageReceived(connection, message)
		}
	}
}
