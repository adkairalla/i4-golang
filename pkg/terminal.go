package pkg

import "regexp"

/**
 * Below are the constants used by terminals in order to display colors and effects
 */

const CNRM = "\x1B[0;0m"  /* "Normal"                            */
const CNUL = ""           /* No Change                           */
const KNRM = "\x1B[0m"    /* Foreground "Normal"                 */
const KBLK = "\x1b[0;30m" /* Foreground Black                    */
const KRED = "\x1B[0;31m" /* Foreground Dark Red                 */
const KGRN = "\x1B[0;32m" /* Foreground Dark Green               */
const KYEL = "\x1B[0;33m" /* Foreground Dark Yellow              */
const KBLU = "\x1B[0;34m" /* Foreground Dark Blue                */
const KMAG = "\x1B[0;35m" /* Foreground Dark Magenta             */
const KCYN = "\x1B[0;36m" /* Foreground Dark Cyan                */
const KWHT = "\x1B[0;37m" /* Foreground Dark White (Light Gray)  */
const KNUL = ""           /* Foreground No Change                */
const BBLK = "\x1B[1;30m" /* Foreground Bright Black (Dark Gray) */
const BRED = "\x1B[1;31m" /* Foreground Bright Red               */
const BGRN = "\x1B[1;32m" /* Foreground Bright Green             */
const BYEL = "\x1B[1;33m" /* Foreground Bright Yellow            */
const BBLU = "\x1B[1;34m" /* Foreground Bright Blue              */
const BMAG = "\x1B[1;35m" /* Foreground Bright Magenta           */
const BCYN = "\x1B[1;36m" /* Foreground Bright Cyan              */
const BWHT = "\x1B[1;37m" /* Foreground Bright White             */

const BKBLK = "\x1B[40m" /* Background Black                    */
const BKRED = "\x1B[41m" /* Background Dark Red                 */
const BKGRN = "\x1B[42m" /* Background Dark Green               */
const BKYEL = "\x1B[43m" /* Background Dark Yellow              */
const BKBLU = "\x1B[44m" /* Background Dark Blue                */
const BKMAG = "\x1B[45m" /* Background Dark Magenta             */
const BKCYN = "\x1B[46m" /* Background Dark Cyan                */
const BKWHT = "\x1B[47m" /* Background Dark White (Light Gray)  */

const FBLK = "\x1B[5;30m" /* Foreground Flashing Black (silly)   */
const FRED = "\x1B[5;31m" /* Foreground Flashing Dark Red        */
const FGRN = "\x1B[5;32m" /* Foreground Flashing Dark Green      */
const FYEL = "\x1B[5;33m" /* Foreground Flashing Dark Yellow     */
const FBLU = "\x1B[5;34m" /* Foreground Flashing Dark Blue       */
const FMAG = "\x1B[5;35m" /* Foreground Flashing Dark Magenta    */
const FCYN = "\x1B[5;36m" /* Foreground Flashing Dark Cyan       */
const FWHT = "\x1B[5;37m" /* Foreground Flashing Light Gray      */

const BFBLK = "\x1B[1;5;30m" /* Foreground Flashing Dark Gray       */
const BFRED = "\x1B[1;5;31m" /* Foreground Flashing Bright Red      */
const BFGRN = "\x1B[1;5;32m" /* Foreground Flashing Bright Green    */
const BFYEL = "\x1B[1;5;33m" /* Foreground Flashing Bright Yellow   */
const BFBLU = "\x1B[1;5;34m" /* Foreground Flashing Bright Blue     */
const BFMAG = "\x1B[1;5;35m" /* Foreground Flashing Bright Magenta  */
const BFCYN = "\x1B[1;5;36m" /* Foreground Flashing Bright Cyan     */
const BFWHT = "\x1B[1;5;37m" /* Foreground Flashing Bright White    */

const CBEEP = "\x07"
const CAT = "@@"
const CAMP = "&"
const CSLH = "\\"

const CCLR = "\x1b[0m" /* Reset color code */
const CBLD = "\x1b[1m" /* Bold color code */
const CFNT = "\x1b[2m" /* Faint color mode */
const CUDL = "\x1B[4m" /* Underline ANSI code */
const CFSH = "\x1B[5m" /* Flashing ANSI code. */
const CRVS = "\x1B[7m" /* Reverse video ANSI code */

/* conditional color.  pass it a pointer to a char_data and a color level. */
const C_OFF = 0
const C_SPR = 1
const C_NRM = 2
const C_CMP = 3

/**
 * ColorMap maps the constants to the string that should be substituted
 */
var ColorMap = map[string]string{
	"{n": KNRM,

	//Foreground colors
	"{l": KBLK,
	"{r": KRED,
	"{g": KGRN,
	"{y": KYEL,
	"{b": KBLU,
	"{m": KMAG,
	"{c": KCYN,
	"{w": KWHT,

	//Foreground bold colors
	"{L": BBLK,
	"{R": BRED,
	"{G": BGRN,
	"{Y": BYEL,
	"{B": BBLU,
	"{M": BMAG,
	"{C": BCYN,
	"{W": BWHT,

	//Effects
	"{f": CFSH,
	"{u": CUDL,

	//Background colors
	"{1": BKRED,
	"{2": BKGRN,
	"{3": BKYEL,
	"{4": BKBLU,
	"{5": BKMAG,
	"{6": BKCYN,
	"{7": BKWHT,
}

/**
 * Verifies that it is a valid color code
 */
var colorExpression = regexp.MustCompile(`\{[nlrgybmcwfuLRGYBMCW1234567]`)

/**
 * translates a color string to the correct color code
 */
func colorCode(message string) string {
	return ColorMap[message]
}

/**
 * Passes a string through a parser exchanging the color strings to the appropriate codes
 */
func Colorize(message string) string {
	return colorExpression.ReplaceAllStringFunc(message, colorCode) + CCLR
}
