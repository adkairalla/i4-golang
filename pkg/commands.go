package pkg

import (
	"errors"
	"strings"

	"github.com/sirupsen/logrus"
)

//Command is the structure for a player-issued command
type Command struct {
	command         string
	Closure         func(char *Character, arguments []string, cmd, subcmd int)
	MinimumPosition int
	SubCommand      int
	Flags           Bitflag
}

var commandKeys []string

//Sort all command keys into a reusable lookup
func prepareCommands() {
	for _, cmd := range commandList {
		commandKeys = append(commandKeys, cmd.command)
	}
}

//GetCommand receives some user input, return the related command
func GetCommand(userInput string) (*Command, error) {
	logrus.Trace("Get command debugging: userInput: " + userInput)
	for idx, k := range commandKeys {
		//logrus.Trace(commandKeys)
		if strings.HasPrefix(k, userInput) {
			//logrus.Trace(commandList)
			return commandList[idx], nil
		}
	}

	return nil, errors.New("Sorry, I didn't understand that. Try again?\n")
}

//Without further ado, the command list
var commandList = []*Command{
	// directions should be first to match single letter commands like 'n', 'w', etc
	{"north", doMove, POS_STANDING, DIR_NORTH, 0},
	{"south", doMove, POS_STANDING, DIR_SOUTH, 0},
	{"west", doMove, POS_STANDING, DIR_WEST, 0},
	{"east", doMove, POS_STANDING, DIR_EAST, 0},
	{"up", doMove, POS_STANDING, DIR_UP, 0},
	{"down", doMove, POS_STANDING, DIR_DOWN, 0},

	// rest of commands in the order you feel like it BRO!
	{"drop", doDrop, POS_SITTING, 0, 0},
	{"exits", doExits, POS_RESTING, 0, 0},
	{"equipment", doEquipment, POS_DEAD, 0, 0},
	{"get", doGet, POS_SITTING, 0, 0},
	{"give", doGive, POS_SITTING, 0, 0},
	{"hit", doHit, POS_STANDING, 0, 0},
	{"inventory", doInventory, POS_DEAD, 0, 0},
	{"kill", doKill, POS_STANDING, 0, 0},
	{"look", doLook, POS_STANDING, 0, 0},
	{"remove", doRemove, POS_SITTING, 0, 0},
	{"wear", doWear, POS_SITTING, 0, 0},
}
