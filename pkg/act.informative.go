package pkg

var doLook = func(char *Character, arguments []string, cmd, subcmd int) {
	if currentRoom := char.CurrentRoom; len(arguments) == 0 && currentRoom != nil {
		currentRoom.ShowTo(char)
	} else if len(arguments) > 0 {
		char.SendToCharacter("look at/from/in <target> not implemented yet\n")
	} else {
		char.SendToCharacter("Rooms not implemented yet\n")
	}
}

var doExits = func(char *Character, arguments []string, cmd, subcmd int) {
	if currentRoom := char.CurrentRoom; len(arguments) == 0 && currentRoom != nil {
		currentRoom.listExitsTo(char)
		char.SendToCharacter("\n")
	} else {
		char.SendToCharacter("There are no exits from where you are now. *SCARY*.\n")
	}
}

var doEquipment = func(char *Character, arguments []string, cmd, subcmd int) {
	char.equipList()
}
