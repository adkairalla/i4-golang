package pkg

func (world *World) GetRoom(id uint32) *Room {
	return world.roomList[id]
}
