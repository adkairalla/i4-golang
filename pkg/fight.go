package pkg

import (
	"fmt"

	"github.com/sirupsen/logrus"
)

func hit(char *Character, target *Character) {

	if char.CurrentRoom != target.CurrentRoom {
		logrus.Error("Different rooms! Char:", char.CurrentRoom, "Target:", target.CurrentRoom)
		char.disengageCombat(target)
	}

	dice := RandomInt(100)

	if dice >= 50 {
		damage(char, target)
	} else {
		char.SendToCharacter("You try to hit " + target.Name + " but you miss.\n")
		target.SendToCharacter(fmt.Sprintf("%s tries to hit you but misses.\n", char.Name))
	}

}

func damage(char *Character, target *Character) {
	dam := RandomInt(30)

	char.SendToCharacter(fmt.Sprintf("You hit %s for %d.\n", target.Name, dam))
	target.SendToCharacter(fmt.Sprintf("%s hits you for %d.\n", char.Name, dam))
	target.currentHit -= dam

	target.updatePosition()

	switch target.position {
	case POS_MORTALLYW:
		//act("$n is mortally wounded, and will die soon, if not aided.", TRUE, victim, 0, 0, TO_ROOM);
		target.SendToCharacter("You are mortally wounded, and will die soon, if not aided.\r\n")
	case POS_INCAP:
		//act("$n is incapacitated and will slowly die, if not aided.", TRUE, victim, 0, 0, TO_ROOM);
		target.SendToCharacter("You are incapacitated and will slowly die, if not aided.\r\n")
	case POS_STUNNED:
		//act("$n is stunned, but will probably regain consciousness again.", TRUE, victim, 0, 0, TO_ROOM);
		target.SendToCharacter("You're stunned, but will probably regain consciousness again.\r\n")
	case POS_DEAD:
		//act("$n is dead!  R.I.P.", FALSE, victim, 0, 0, TO_ROOM);
		target.SendToCharacter("You are dead!  Sorry...\r\n")
	default: /* >= POSITION SLEEPING */
		if dam > (target.baseHit + target.modHit/4) {
			target.SendToCharacter("That really did HURT!\r\n")
		}
		if target.currentHit < (target.baseHit + target.modHit/4) {
			target.SendToCharacter("You wish that your wounds would stop BLEEDING so much!%s\r\n")
			// if char != target && MOB_FLAGGED(victim, MOB_WIMPY) {
			// 	do_flee(victim, NULL, 0, 0)
			// }
		}
		// if character.combatVariables && (victim != ch) &&
		// 	target.currentHit < GET_WIMP_LEV(victim) && target.currentHit > 0 {
		// 	target.SendToCharacter(victim, "You wimp out, and attempt to flee!\r\n")
		// 	do_flee(victim, NULL, 0, 0)
		// }
	}
	/* stop someone from fighting if they're stunned or worse */
	if target.position <= POS_STUNNED {
		char.disengageCombat(target)
		target.emptyCombatList()
	}

	if target.position == POS_DEAD {
		char.SendToCharacter("You killed " + target.Name + ". You monster....")
		target.SendToCharacter("You are D-E-D dead! Killed by " + char.Name)
	}
}

func performViolence() {
	for _, char := range DAHWORUDO.combatList {
		if len(char.combatList) == 0 {
			var res bool
			DAHWORUDO.combatList, res = removeCharacterFromList(DAHWORUDO.combatList, char)
			logrus.Trace("removing", char.Name, "from global combat list:", res)
		}

		if char.combatVariables.attackTimer > 0 {
			char.combatVariables.attackTimer--
			continue
		}

		if char.position < POS_FIGHTING {
			char.SendToCharacter("You can't fight while sitting!!\n")
			continue
		}

		char.combatVariables.attackTimer = 35

		if len(char.combatList) == 0 {
			logrus.Error("Combat target nil in hit function!!!")
			return
		}
		logrus.Trace(char.Name, " calling hit on ", char.combatList[0].opponent.Name)

		hit(char, char.combatList[0].opponent)
	}

}
