package pkg

/**
 * NPC represents the general structure of a character with the extra data of NPC's
 */
type Npc struct {
	Character
}
