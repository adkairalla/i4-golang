package pkg

type Combat struct {
	opponent *Character
	distance uint8
	threat   uint32
}

type CombatVariables struct {
	attackTimer uint32
	wimpy       uint32
}
