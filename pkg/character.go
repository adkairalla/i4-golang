package pkg

import (
	"errors"
	"fmt"
	"strings"

	"github.com/sirupsen/logrus"
)

/**
 * Character represents the general structure of a character (Player or Npc)
 */
type Character struct {
	Name string
	*Connection
	CurrentRoom       *Room
	output            string
	baseHit           int64
	modHit            int64
	currentHit        int64
	baseMana          int64
	modMana           int64
	currentMana       int64
	baseEnergy        int64
	modEnergy         int64
	currentEnergy     int64
	position          int8
	short_description []string
	inventory         []*Object
	equipment         [NUM_WEARS]*Object
	combatList        []*Combat
	combatVariables   CombatVariables
}

const (
	POS_DEAD      = 1 + iota /**< Character position = dead */
	POS_MORTALLYW            /**< Character position = mortally wounded */
	POS_INCAP                /**< Character position = incapacitaded */
	POS_STUNNED              /**< Character position = stunned */
	POS_SLEEPING             /**< Character position = sleeping */
	POS_RESTING              /**< Character position = resting */
	POS_SITTING              /**< Character position = sitting */
	POS_FIGHTING             /**< Character position = fighting */
	POS_STANDING             /**< Character position = standing */
)

const (
	/* Take/Wear flags: used by obj_data.obj_flags.wear_flags */
	WEAR_HEAD      = iota /**< Item can be worn on head 		*/
	WEAR_BODY             /**< Item can be worn on body 		*/
	WEAR_ARMS             /**< Item can be worn on arms 		*/
	WEAR_LEGS             /**< Item can be worn on legs 		*/
	WEAR_HANDS            /**< Item can be worn on hands		*/
	WEAR_WAIST            /**< Item can be worn around waist 	*/
	WEAR_WRIST_R          /**< Item can be worn on wrist 		*/
	WEAR_WRIST_L          /**< Item can be worn on wrist 		*/
	WEAR_FEET             /**< Item can be worn on feet 		*/
	WEAR_FINGER_R         /**< Item can be worn on finger 		*/
	WEAR_FINGER_L         /**< Item can be worn on finger 		*/
	WEAR_NECK_1           /**< Item can be worn around neck 	*/
	WEAR_NECK_2           /**< Item can be worn around neck 	*/
	WEAR_EAR_R            /**< Item can be worn on ear 			*/
	WEAR_EAR_L            /**< Item can be worn on ear 			*/
	WEAR_MAIN_HAND        /**< Item can be wielded 				*/
	WEAR_OFFHAND          /**< Item can be used as a shield 	*/
	WEAR_LIGHT            /**< Item is a floating light			*/
	WEAR_TRINKET          /**< Item is worn as a trinket 		*/
	WEAR_ABOUT            /**< Item can be worn about body 		*/
	/** Total number of item wears */
	NUM_WEARS
)

//Finds an item in the inventory list by name
func (char *Character) findObjectsInventoryByName(target string) (*Object, error) {
	for _, obj := range char.inventory {
		if ContainsString(obj.short_description, target) {
			return obj, nil
		}
	}
	return nil, errors.New("Item not found.\n")
}

//Appends a character to the characters list
func (char *Character) addObjectInventory(obj *Object) {
	char.inventory = append(char.inventory, obj)
}

//Removes a Character from the character list
func (char *Character) removeObjectInventory(obj *Object) {
	var aux []*Object
	for _, invItem := range char.inventory {
		if invItem != obj {
			aux = append(aux, invItem)
		}
	}
	char.inventory = aux
}

//Finds an equipped object at the equipmen list by name
func (char *Character) findEquippedObjectsByName(target string) (*Object, error) {
	for _, obj := range char.equipment {
		if obj != nil && ContainsString(obj.short_description, target) {
			return obj, nil
		}
	}
	return nil, errors.New("Item not found.\n")
}

/**
 * Removes an equipment from the equipment character list
 */
func (char *Character) removeEquip(obj *Object) {
	var aux [NUM_WEARS]*Object
	for idx, objEquip := range char.equipment {
		if objEquip != obj {
			aux[idx] = objEquip
		}
	}
	char.equipment = aux
}

//CanSeeHidden checks if player can see something/someone that is hidden
func (char *Character) CanSeeHidden(diff uint8) bool {
	// TODO: implement the actual check to char's skill/stat
	return true
}

//Checks if the player can see something/someone that is invisible
func (char *Character) CanSeeInvis() bool {
	// TODO: implement the actual check to char's skills/flags/buffs
	return true
}

//SendToCharacter adds 'text' to the output buffer of the char
func (char *Character) SendToCharacter(text string) {
	if char.Connection == nil {
		return
	}
	char.output += text
}

//HasOutput returns true if char has any text left to be sent over to the client
func (char *Character) HasOutput() bool { return char.output != "" }

//GetOutput return the text in the char output buffer
func (char *Character) GetOutput() string {
	return Colorize(char.output)
}

//ClearOutput empties out the char output buffer
func (char *Character) ClearOutput() { char.output = "" }

//Sends char the command prompt after a command is received
func (char *Character) SendPrompt() {
	//Create prompt styling
	char.SendToCharacter(Colorize(fmt.Sprintf("\n{R%dh/{r%dH {B%dm/{b%dM {G%de/{g%dE>",
		char.currentHit, char.baseHit+char.modHit,
		char.currentMana, char.baseMana+char.modMana,
		char.currentEnergy, char.baseEnergy+char.modEnergy)))
}

//Execute a command on behalf of this char
func (char *Character) do(verb string, arguments []string) {
	command, err := GetCommand(verb)
	if err != nil {
		char.output += fmt.Sprint(err)
		return
	}

	command.Closure(char, arguments, command.MinimumPosition, command.SubCommand)
}

//Lists the char inventory
func (char *Character) listInventory() {
	char.output += "You are carrying:\n"
	if len(char.inventory) == 0 {
		char.output += "Nothing!\n"
	}
	for _, obj := range char.inventory {
		char.output += obj.name + "\n"
	}
}

//Checks the slot the item should be equipped and equips it
func (char *Character) equipItem(equip *Object) {
	itemSlot := equipToItemMap[int(equip.wear)]
	if itemSlot == WEAR_WRIST_R && char.equipment[itemSlot] != nil {
		itemSlot = WEAR_WRIST_L
	} else if itemSlot == WEAR_EAR_R && char.equipment[itemSlot] != nil {
		itemSlot = WEAR_EAR_L
	} else if itemSlot == WEAR_FINGER_R && char.equipment[itemSlot] != nil {
		itemSlot = WEAR_FINGER_L
	} else if itemSlot == WEAR_NECK_1 && char.equipment[itemSlot] != nil {
		itemSlot = WEAR_NECK_2
	} else if itemSlot == WEAR_MAIN_HAND && char.equipment[itemSlot] != nil {
		itemSlot = WEAR_OFFHAND
	}
	if char.equipment[itemSlot] == nil {
		char.equipment[itemSlot] = equip
		char.removeObjectInventory(equip)
		char.SendToCharacter("You wear " + equip.name + " on your " + strings.ToLower(equipMap[itemSlot]))
	} else {
		char.SendToCharacter("You already have something equipped on your " + equipMap[itemSlot])
	}
}

//Removes an item from the equipped list and moves it to the char inventory
func (char *Character) unequipItem(equip *Object) {
	var itemSlot int
	for idx, equipObj := range char.equipment {
		if equip == equipObj {
			itemSlot = idx
			break
		}
	}
	char.removeEquip(equip)
	char.addObjectInventory(equip)
	char.SendToCharacter("You remove " + equip.name + " from your " + strings.ToLower(equipMap[itemSlot]) + ".")
}

//Prints the list of equipment to the char
func (char *Character) equipList() {
	printed := false
	for idx, obj := range char.equipment {
		if obj != nil {
			char.SendToCharacter(fmt.Sprintf("<%s>\t\t%s\n", equipMap[idx], obj.name))
			printed = true
		}
	}
	if !printed {
		char.SendToCharacter("You are wearing nothing.\n")
	}
}

//Engages character in combat with target and adds to World list
func (char *Character) engageCombat(target *Character) {
	combat := &Combat{
		opponent: target,
		threat:   0,
		distance: 0,
	}

	char.combatList = append(char.combatList, combat)
	if !isCharacterInList(DAHWORUDO.combatList, char) {
		char.combatVariables.attackTimer = 10
		DAHWORUDO.combatList = addCharacterToList(DAHWORUDO.combatList, char)
	}

	if !target.searchCombatList(char) {
		combat2 := &Combat{
			opponent: char,
			threat:   0,
			distance: 0,
		}
		target.combatList = append(target.combatList, combat2)
		if !isCharacterInList(DAHWORUDO.combatList, target) {
			target.combatVariables.attackTimer = 35
			DAHWORUDO.combatList = addCharacterToList(DAHWORUDO.combatList, target)
		}
	}

}

func (char *Character) searchCombatList(target *Character) bool {
	for _, targetList := range char.combatList {
		if targetList.opponent == target {
			return true
		}
	}
	return false
}

func (char *Character) disengageCombat(target *Character) {
	logrus.Trace("disengageCombat Called")
	var aux []*Combat
	for _, targetList := range char.combatList {
		if targetList.opponent != target {
			aux = append(aux, targetList)
		}
	}
	char.combatList = aux
	if len(char.combatList) <= 0 {
		DAHWORUDO.combatList, _ = removeCharacterFromList(DAHWORUDO.combatList, char)
	}
}

func (char *Character) emptyCombatList() {
	logrus.Trace("disengageCombat Called")
	char.combatList = nil
	DAHWORUDO.combatList, _ = removeCharacterFromList(DAHWORUDO.combatList, char)
}

func (char *Character) updatePosition() {
	if char.currentHit > 0 && char.position > POS_STUNNED {
		return
	} else if char.currentHit > 0 {
		char.position = POS_STANDING
	} else if char.currentHit <= -11 {
		char.position = POS_DEAD
	} else if char.currentHit <= -6 {
		char.position = POS_MORTALLYW
	} else if char.currentHit <= -3 {
		char.position = POS_INCAP
	} else {
		char.position = POS_STUNNED
	}
}
