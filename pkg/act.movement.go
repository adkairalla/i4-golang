package pkg

import (
	"errors"

	"github.com/sirupsen/logrus"
)

const (
	DIR_NORTH = 0 + iota
	DIR_EAST
	DIR_SOUTH
	DIR_WEST
	DIR_UP
	DIR_DOWN
)

const MAX_DIRS = DIR_DOWN + 1

var dirNamesMap = func() map[uint8]string {
	return map[uint8]string{
		DIR_NORTH: "North",
		DIR_EAST:  "East",
		DIR_SOUTH: "South",
		DIR_WEST:  "West",
		DIR_UP:    "Up",
		DIR_DOWN:  "Down",
	}
}

func DirectionName(key uint8) string {
	return dirNamesMap()[key]
}

var doMove = func(char *Character, arguments []string, cmd, direction int) {
	if char.CurrentRoom == nil {
		logrus.Error("Character is NOWHERE and trying to move. Wtf?")
	}

	char.PerformMove(uint8(direction))
}

func (char *Character) CanMove(dir uint8) (*RoomExit, error) {
	exit := char.CurrentRoom.exits[dir]

	if exit == nil {
		return nil, errors.New("There's nothing that way.\n")
	}

	// first we check if the user can see the exit
	if exit.flags.HasFlag(ROOMEXIT_HIDDEN) {
		// TODO: add actual check :)
	}
	if exit.flags.HasFlag(ROOMEXIT_INVISIBLE) {
		// TODO: add actual check :)
	}

	if exit.flags.HasFlag(ROOMEXIT_LOCKED) {
		return nil, errors.New(CaptalizeFirst(exit.name) + "is locked. You cannot move in that direction.")
	} else if exit.flags.HasFlag(ROOMEXIT_CLOSED) {
		return nil, errors.New("You cannot move in that direction.")
	}

	return exit, nil
}

func (char *Character) PerformMove(dir uint8) {
	// check if the character can move in that direction
	exit, err := char.CanMove(dir)

	if err != nil {
		char.SendToCharacter(err.Error())
		return
	}

	toRoom := DAHWORUDO.GetRoom(exit.toRoomId)

	// move the character and if it's a char show them the room
	char.CurrentRoom.removeCharacter(char)
	char.CurrentRoom = toRoom
	char.CurrentRoom.addCharacter(char)
	toRoom.ShowTo(char)
}
