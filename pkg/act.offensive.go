package pkg

var doHit = func(char *Character, arguments []string, cmd, subcmd int) {
	//TODO if multiple instances are in the same room, need to add treatment for 1.mob 2.mob etc, OR
	//if we're going for the newbie12345 option, no need for that, but need to list and add the VRnum to the short description list

	if len(arguments) == 0 {
		char.SendToCharacter("Hit Who?\n")
		return
	}

	if arguments[0] == "me" || arguments[0] == "self" {
		char.SendToCharacter("You little masochist!\n")
		return
	}

	//findCharacterByName needs to be changed not to be case sensitive and to accept partial names
	target, err := DAHWORUDO.roomList[char.CurrentRoom.id].findCharacterByName(arguments[0])
	if err != nil {
		char.SendToCharacter(err.Error())
		return
	}

	if target == char {
		char.SendToCharacter("You little masochist!\n")
		return
	}

	hit(char, target)
	char.engageCombat(target)
}

var doKill = func(char *Character, arguments []string, cmd, subcmd int) {
	//TODO kill actually kills if you're a god and does a bunch of checks
	doHit(char, arguments, cmd, subcmd)
}
