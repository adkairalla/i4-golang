package pkg

import (
	"fmt"

	"github.com/sirupsen/logrus"
)

var equipMap = map[int]string{
	WEAR_HEAD:      "Head",
	WEAR_BODY:      "Body",
	WEAR_ARMS:      "Arms",
	WEAR_LEGS:      "Legs",
	WEAR_HANDS:     "Hands",
	WEAR_WAIST:     "Waist",
	WEAR_WRIST_R:   "Right Wrist",
	WEAR_WRIST_L:   "Left Wrist",
	WEAR_FEET:      "Feet",
	WEAR_FINGER_R:  "Right Finger",
	WEAR_FINGER_L:  "Left Finger",
	WEAR_NECK_1:    "Neck",
	WEAR_NECK_2:    "Neck",
	WEAR_EAR_R:     "Right Ear",
	WEAR_EAR_L:     "Left Ear",
	WEAR_MAIN_HAND: "Main Hand",
	WEAR_OFFHAND:   "Off Hand",
	WEAR_LIGHT:     "Light Orb",
	WEAR_TRINKET:   "Trinket",
	WEAR_ABOUT:     "About Body",
}

var equipToItemMap = map[int]int{
	ITEM_WEAR_HEAD:      WEAR_HEAD,
	ITEM_WEAR_BODY:      WEAR_BODY,
	ITEM_WEAR_ARMS:      WEAR_ARMS,
	ITEM_WEAR_LEGS:      WEAR_LEGS,
	ITEM_WEAR_HANDS:     WEAR_HANDS,
	ITEM_WEAR_WAIST:     WEAR_WAIST,
	ITEM_WEAR_WRIST:     WEAR_WRIST_R,
	ITEM_WEAR_FEET:      WEAR_FEET,
	ITEM_WEAR_FINGER:    WEAR_FINGER_R,
	ITEM_WEAR_NECK:      WEAR_NECK_1,
	ITEM_WEAR_EAR:       WEAR_EAR_R,
	ITEM_WEAR_MAIN_HAND: WEAR_MAIN_HAND,
	ITEM_WEAR_OFFHAND:   WEAR_OFFHAND,
	ITEM_WEAR_LIGHT:     WEAR_LIGHT,
	ITEM_WEAR_TRINKET:   WEAR_TRINKET,
	ITEM_WEAR_ABOUT:     WEAR_ABOUT,
}

var doGet = func(char *Character, arguments []string, cmd, subcmd int) {
	//TODO if multiple instances are in the same room, need to add treatment for 1.item 2.item etc, OR
	//if we're going for the item12345 option, no need for that, but need to list and add the VRnum to the short description list

	if len(arguments) == 0 {
		char.SendToCharacter("Get what?\n")
		return
	}

	target, err := DAHWORUDO.roomList[char.CurrentRoom.id].findObjectsByName(arguments[0])
	if err != nil {
		char.SendToCharacter(err.Error())
		return
	}

	logrus.Debug(fmt.Sprintf("Objects: %v\n", DAHWORUDO.roomList[char.CurrentRoom.id].objects))
	char.SendToCharacter("You get " + target.name + "\n")
	char.addObjectInventory(target)
	DAHWORUDO.roomList[char.CurrentRoom.id].removeObject(target)
	logrus.Debug(fmt.Sprintf("Removed Object: %v\n", target))
}

var doDrop = func(char *Character, arguments []string, cmd, subcmd int) {
	//TODO if multiple instances are in the same room, need to add treatment for 1.item 2.item etc, OR
	//if we're going for the item12345 option, no need for that, but need to list and add the VRnum to the short description list

	if len(arguments) == 0 {
		char.SendToCharacter("Drop what?\n")
		return
	}

	target, err := char.findObjectsInventoryByName(arguments[0])
	if err != nil {
		char.SendToCharacter(err.Error())
		return
	}

	char.SendToCharacter("You drop " + target.name + "\n")
	char.removeObjectInventory(target)
	DAHWORUDO.roomList[char.CurrentRoom.id].addObject(target)
}

var doJunk = func(char *Character, arguments []string, cmd, subcmd int) {
	if len(arguments) == 0 {
		char.SendToCharacter("Junk what?\n")
		return
	}

	target, err := char.findObjectsInventoryByName(arguments[0])
	if err != nil {
		char.SendToCharacter(err.Error())
		return
	}

	char.SendToCharacter("You junk " + target.name + "\nIt vanishes in a puff of smoke!\n")
	char.removeObjectInventory(target)
}

var doGive = func(char *Character, arguments []string, cmd, subcmd int) {
	if len(arguments) < 2 {
		char.SendToCharacter("Give what to who?\n")
		return
	}

	targetObj, err := char.findObjectsInventoryByName(arguments[0])
	if err != nil {
		char.SendToCharacter(err.Error())
		return
	}

	targetCharacter, err := DAHWORUDO.GetRoom(char.CurrentRoom.id).findCharacterByName(arguments[1])
	if err != nil {
		char.SendToCharacter(err.Error())
		return
	}

	char.SendToCharacter("You give " + targetObj.name + " to " + targetCharacter.Name + ".\n")
	char.removeObjectInventory(targetObj)
	targetCharacter.addObjectInventory(targetObj)
}

var doInventory = func(char *Character, arguments []string, cmd, subcmd int) {
	//TODO Add options to see the codes, subnames of the item etc
	//TODO check for item flags for invisibility
	char.listInventory()
}

var doWear = func(char *Character, arguments []string, cmd, subcmd int) {

	if len(arguments) == 0 {
		char.SendToCharacter("Wear what?\n")
		return
	}

	targetObj, err := char.findObjectsInventoryByName(arguments[0])
	if err != nil {
		char.SendToCharacter(err.Error())
		return
	}

	//TODO if equip all, go through inventory trying to equip all of them

	if !targetObj.category.HasFlag(ITEM_ARMOR | ITEM_WEAPON | ITEM_LIGHT | ITEM_STAFF | ITEM_WAND) {
		char.SendToCharacter("This item is not an equipment")
		return
	}

	//TODO Find restrictions of the item and return correct message (not applicable yet)

	char.equipItem(targetObj)

	//TODO if equip all, go back for next iteration (better to have two separado functions)

}

var doRemove = func(char *Character, arguments []string, cmd, subcmd int) {
	if len(arguments) == 0 {
		char.SendToCharacter("Remove what?\n")
		return
	}

	targetObj, err := char.findEquippedObjectsByName(arguments[0])
	if err != nil {
		char.SendToCharacter(err.Error())
		return
	}

	char.unequipItem(targetObj)
}
