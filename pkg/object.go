package pkg

//Object represent prototypes of objects in the game.
type Object struct {
	id                 uint32
	category           Bitflag  /**< Object type information */
	wear               Bitflag  /** < Object wearable information */
	affected           Bitflag  /**< affects */
	name               string   /**< Keyword reference(s) for object. */
	description        string   /**< Shown when the object is lying in a room. */
	short_description  []string /**< Shown when worn, carried, in a container */
	action_description string   /**< Displays when (if) the object is used */
}

const (
	/* object-related defines */
	/* Item types: used by category flags */
	ITEM_OTHER     = iota + 1 /**< Misc object						*/
	ITEM_MONEY                /**< Item is money (gold)				*/
	ITEM_TREASURE             /**< Item is a treasure, not gold		*/
	ITEM_TRASH                /**< Trash - shopkeepers won't buy	*/
	ITEM_CONTAINER            /**< Item is a container				*/
	ITEM_FURNITURE            /**< Sittable Furniture				*/
	ITEM_NOTE                 /**< Item is note 					*/
	ITEM_SCROLL               /**< Item is a scroll					*/
	ITEM_POTION               /**< Item is a potion					*/
	ITEM_WAND                 /**< Item is a wand					*/
	ITEM_STAFF                /**< Item is a staff					*/
	ITEM_LIGHT                /**< Item is a light source			*/
	ITEM_WEAPON               /**< Item is a weapon					*/
	ITEM_ARMOR                /**< Item is armor					*/
	ITEM_KEY                  /**< Item is a key					*/
	ITEM_DRINKCON             /**< Item is a drink container		*/
	ITEM_FOOD                 /**< Item is food						*/
	ITEM_BOAT                 /**< Item is a boat					*/
	ITEM_FOUNTAIN             /**< Item is a fountain				*/
	/** Total number of item types.*/
	NUM_ITEM_TYPES
)

const (
	/* Take/Wear flags: used by obj_data.obj_flags.wear_flags */
	ITEM_WEAR_TAKE      = iota + 1 /**< Item can be taken 				*/
	ITEM_WEAR_HEAD                 /**< Item can be worn on head 		*/
	ITEM_WEAR_BODY                 /**< Item can be worn on body 		*/
	ITEM_WEAR_ARMS                 /**< Item can be worn on arms 		*/
	ITEM_WEAR_LEGS                 /**< Item can be worn on legs 		*/
	ITEM_WEAR_HANDS                /**< Item can be worn on hands		*/
	ITEM_WEAR_WAIST                /**< Item can be worn around waist 	*/
	ITEM_WEAR_WRIST                /**< Item can be worn on wrist 		*/
	ITEM_WEAR_FEET                 /**< Item can be worn on feet 		*/
	ITEM_WEAR_FINGER               /**< Item can be worn on finger 		*/
	ITEM_WEAR_NECK                 /**< Item can be worn around neck 	*/
	ITEM_WEAR_EAR                  /**< Item can be worn on ear 		*/
	ITEM_WEAR_MAIN_HAND            /**< Item can be wielded 			*/
	ITEM_WEAR_OFFHAND              /**< Item can be used as a shield 	*/
	ITEM_WEAR_LIGHT                /**< Item is a floating light		*/
	ITEM_WEAR_TRINKET              /**< Item is worn as a trinket 		*/
	ITEM_WEAR_ABOUT                /**< Item can be worn about body 	*/
	/** Total number of item wears */
	NUM_ITEM_WEARS
)

const (
	/* Extra object flags: used by obj_data.obj_flags.extra_flags */
	ITEM_NODONATE  = iota + 1 /**< Item cannot be donated 		*/
	ITEM_NOINVIS              /**< Item cannot be made invis	*/
	ITEM_INVISIBLE            /**< Item is invisible 			*/
	ITEM_MAGIC                /**< Item is magical 				*/
	ITEM_BLESS                /**< Item is blessed 				*/
	ITEM_NOSELL               /**< Shopkeepers won't touch it 	*/
	ITEM_QUEST                /**< Item is a quest item         */
	/** Total number of item flags */
	NUM_ITEM_FLAGS
)

//Override to print object names when object is used in a string
func (obj Object) String() string {
	return obj.name
}

//Nothing represents an invalid object ID
const NOTHING = ^uint32(0)
