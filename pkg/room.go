package pkg

import (
	"errors"
	"fmt"
	"strings"
)

/**
 * RoomExit represents the structure of an exit in a room
 */
type RoomExit struct {
	direction   uint8   // direction of the exit
	toRoomId    uint32  // id of the room that this exit leads to
	name        string  // name of the door/passage
	description string  // roleplay description of the exit
	flags       Bitflag // flags of the exit (locked, closed, hidden, etc)
}

/**
 * Room represents the general structure of rooms in the game
 */
type Room struct {
	id          uint32       // id of the room in the roomList map of the world
	flags       Bitflag      // flags of the room (dark, peaceful, etc)
	title       string       // room name/title
	description string       // room roleplay description
	characters  []*Character // list of character present in the room
	objects     []*Object    // list of objects present in the room
	exits       []*RoomExit  // map of exits this room has
}

// NOWHERE represents an invalid location
const NOWHERE = ^uint32(0)

// Flags of a RoomExit
const (
	ROOMEXIT_CLOSED = 0 << iota
	ROOMEXIT_LOCKED
	ROOMEXIT_HIDDEN    // hidden by physical mundane stuff like behind a rock
	ROOMEXIT_INVISIBLE // invisible by means of magic stuff
	ROOMEXIT_BROKEN    // someone already broke the passage that was locked/blocked
)

// Flags of a Room
const (
	ROOM_DARK         Bitflag = 1 << iota /**< ROOM_DARK Dark room, light needed to see */
	ROOM_TRAP                             /**< Death trap, instant death */
	ROOM_NOMOB                            /**< MOBs not allowed in room */
	ROOM_INDOORS                          /**< Indoors, no weather */
	ROOM_PEACEFUL                         /**< Violence not allowed	*/
	ROOM_SOUNDPROOF                       /**< Shouts, gossip blocked */
	ROOM_NOTRACK                          /**< Track won't go through */
	ROOM_NOMAGIC                          /**< Magic not allowed */
	ROOM_TUNNEL                           /**< Room for only 1 pers	*/
	ROOM_PRIVATE                          /**< Can't teleport in */
	ROOM_GODROOM                          /**< LVL_GOD+ only allowed */
	ROOM_HOUSE                            /**< (R) Room is a house */
	ROOM_HOUSE_CRASH                      /**< (R) House needs saving */
	ROOM_ATRIUM                           /**< (R) The door to a house */
	ROOM_OLC                              /**< (R) Modifyable/!compress */
	ROOM_BFS_MARK                         /**< (R) breath-first srch mrk */
	ROOM_WORLDMAP                         /**< World-map style maps here */
	ROOM_NO_RND_SPAWN                     /**< No Random Zone spawn here */
	ROOM_NO_SCOUT                         /**< Scouts mobs shouldn't move here */
)

//Shows a room to the char

func (room *Room) ShowTo(char *Character) {
	if room.flags.HasFlag(ROOM_DARK) {
		char.SendToCharacter("It's too dark to see anything.")
	} else {
		char.SendToCharacter(
			fmt.Sprintf("{Y%s\n{w%s\n{CExits:%s%s",
				room.title,
				room.description,
				room.GetShortExits(char),
				room.listContentsTo(char)))
	}
}

//Lists all contents of a room except the character that sent the command
func (room *Room) listContentsTo(char *Character) string {
	str := "\n{y"
	for _, char := range room.characters {
		//if char != &char.Character {
		str += char.Name + " is standing here.\n"
		//}
	}
	str += "{g"
	for _, obj := range room.objects {
		str += obj.name + " is lying on the floor.\n"
	}

	return str
}

//Finds characters in the room by name
//TODO Needs to remove case sensitive comparisson
func (room *Room) findCharacterByName(target string) (*Character, error) {
	for _, characterList := range room.characters {
		if ContainsString(characterList.short_description, target) {
			return characterList, nil
		}
	}
	return nil, errors.New("Target not found.\n")
}

//Appends a character to the room characters list
func (room *Room) addCharacter(character *Character) {
	room.characters = append(room.characters, character)
}

//Removes a Character from the room character list
func (room *Room) removeCharacter(char *Character) {
	var aux []*Character
	for _, roomChar := range room.characters {
		if roomChar != char {
			aux = append(aux, roomChar)
		}
	}
	room.characters = aux
}

//Find objects in the room by name
//TODO Needs to remove case sensitive comparisson
func (room *Room) findObjectsByName(target string) (*Object, error) {
	for _, obj := range room.objects {
		if ContainsString(obj.short_description, target) {
			return obj, nil
		}
	}
	return nil, errors.New("Item not found.\n")
}

//Appends a character to the room characters list
func (room *Room) addObject(obj *Object) {
	room.objects = append(room.objects, obj)
}

//Removes a Character from the room character list
func (room *Room) removeObject(obj *Object) {
	var aux []*Object
	for _, roomObj := range room.objects {
		if roomObj != obj {
			aux = append(aux, roomObj)
		}
	}
	room.objects = aux
}

//Prints the short exits mode to the char
func (room *Room) GetShortExits(char *Character) string {
	var result = " "
	for _, exit := range room.exits {
		if exit != nil && exit.toRoomId != NOWHERE && exit.CharacterCanSee(char) {
			// check if character can see the exit
			if exit.flags.HasFlag(ROOMEXIT_CLOSED) {
				result += "!"
			}
			if exit.flags.HasFlag(ROOMEXIT_LOCKED) {
				result += "*"
			}
			result += strings.ToLower(DirectionName(exit.direction))[0:1] + " "
		}
	}

	return result
}

//listExitsToChar returns a string with a list of exits the character can see
func (room *Room) listExitsTo(char *Character) {
	char.SendToCharacter("\r\nAvailable exits: \r\n")
	for _, exit := range room.exits {
		if exit != nil && exit.toRoomId != NOWHERE && exit.CharacterCanSee(char) {
			char.SendToCharacter(exit.GetShortDescription(char))
		}
	}
}

//Returns exits that are visible to the char
func (exit *RoomExit) CharacterCanSee(char *Character) bool {
	if !exit.flags.HasFlag(ROOMEXIT_INVISIBLE) && !exit.flags.HasFlag(ROOMEXIT_HIDDEN) {
		return true
	}
	if exit.flags.HasFlag(ROOMEXIT_INVISIBLE) && !char.CanSeeInvis() {
		return true
	}
	if exit.flags.HasFlag(ROOMEXIT_HIDDEN) && !char.CanSeeHidden(0) {
		return true
	}

	return false
}

//GetShortDescription returns a short string to describe an exit to another room
func (exit *RoomExit) GetShortDescription(char *Character) string {
	if exit.toRoomId == NOWHERE {
		return "Oops. Where is what you were looking at?"
	}

	var toRoom = DAHWORUDO.GetRoom(exit.toRoomId)
	var desc = " "

	if exit.name == "" {
		desc += DirectionName(exit.direction) + " leads to " + toRoom.title
	} else {
		desc += exit.name + " to the " + DirectionName(exit.direction) + " leads to " + toRoom.title
	}

	if exit.flags.HasFlag(ROOMEXIT_CLOSED) {
		desc += " (closed)"
	}
	if exit.flags.HasFlag(ROOMEXIT_LOCKED) {
		desc += " (locked)"
	}
	if char.CanSeeHidden(0) && exit.flags.HasFlag(ROOMEXIT_HIDDEN) {
		desc += " (hidden)"
	}
	if char.CanSeeInvis() && exit.flags.HasFlag(ROOMEXIT_INVISIBLE) {
		desc += " (invisible)"
	}
	if exit.flags.HasFlag(ROOMEXIT_BROKEN) {
		desc += " (broken)"
	}

	return desc + "\r\n"
}

//GetLookAtDescription returns a verbose string to show to a character that is looking at the exit
func (exit *RoomExit) GetLookAtDescription() string {
	if exit.toRoomId == NOWHERE {
		return "Oops. Where is what you were looking at?"
	}

	var toRoom = DAHWORUDO.GetRoom(exit.toRoomId)
	var description = fmt.Sprintf("%s leads to %s\r\n%s\r\n",
		DirectionName(exit.direction),
		toRoom.title,
		exit.description,
	)

	if exit.name != "" {
		if exit.flags.HasFlag(ROOMEXIT_CLOSED) {
			description += "The " + exit.name + " is closed and you cannot pass. Try opening it first?" // #gandalf feels
		} else {
			description += "The " + exit.name + " is open and you can freely move through it."
		}
	}

	return description
}

//Load rooms from database
//TODO Actually load them from database...
func loadRooms() map[uint32]*Room {
	rooms := make(map[uint32]*Room, 7)
	rooms[0] = &Room{
		id:          0,
		title:       "Temple of Nothing",
		description: "Welcome to the world of nothingness.",
		exits:       make([]*RoomExit, MAX_DIRS),
	}
	rooms[1] = &Room{
		id:          1,
		title:       "Temple of Nothing (The North)",
		description: "Welcome to the north of nothingness.",
		exits:       make([]*RoomExit, MAX_DIRS),
	}
	rooms[2] = &Room{
		id:          2,
		title:       "Temple of Nothing (The East)",
		description: "Welcome to the east of nothingness.",
		exits:       make([]*RoomExit, MAX_DIRS),
	}
	rooms[3] = &Room{
		id:          3,
		title:       "Temple of Nothing (The South)",
		description: "Welcome to the south of nothingness.",
		exits:       make([]*RoomExit, MAX_DIRS),
	}
	rooms[4] = &Room{
		id:          4,
		title:       "Temple of Nothing (The West)",
		description: "Welcome to the west of nothingness.",
		exits:       make([]*RoomExit, MAX_DIRS),
	}
	rooms[5] = &Room{
		id:          5,
		title:       "Temple of Nothing (The Up)",
		description: "Welcome to the up of nothingness.",
		exits:       make([]*RoomExit, MAX_DIRS),
	}
	rooms[6] = &Room{
		id:          6,
		title:       "Temple of Nothing (The Down)",
		description: "Welcome to the down of nothingness.",
		exits:       make([]*RoomExit, MAX_DIRS),
	}

	rooms[0].exits[DIR_NORTH] = &RoomExit{direction: DIR_NORTH, toRoomId: 1}
	rooms[0].exits[DIR_EAST] = &RoomExit{direction: DIR_EAST, toRoomId: 2}
	rooms[0].exits[DIR_SOUTH] = &RoomExit{direction: DIR_SOUTH, toRoomId: 3}
	rooms[0].exits[DIR_WEST] = &RoomExit{direction: DIR_WEST, toRoomId: 4}
	rooms[0].exits[DIR_UP] = &RoomExit{direction: DIR_UP, toRoomId: 5}
	rooms[0].exits[DIR_DOWN] = &RoomExit{direction: DIR_DOWN, toRoomId: 6}
	rooms[1].exits[DIR_SOUTH] = &RoomExit{direction: DIR_SOUTH, toRoomId: 0}
	rooms[2].exits[DIR_WEST] = &RoomExit{direction: DIR_WEST, toRoomId: 0}
	rooms[3].exits[DIR_NORTH] = &RoomExit{direction: DIR_NORTH, toRoomId: 0}
	rooms[4].exits[DIR_EAST] = &RoomExit{direction: DIR_EAST, toRoomId: 0}
	rooms[5].exits[DIR_DOWN] = &RoomExit{direction: DIR_DOWN, toRoomId: 0}
	rooms[6].exits[DIR_UP] = &RoomExit{direction: DIR_UP, toRoomId: 0}

	npc := &Npc{
		Character{
			Name:              "The evil mob",
			short_description: []string{"evil", "mob", "cretin"},
			CurrentRoom:       rooms[0],
			baseHit:           100,
			modHit:            50,
			currentHit:        150,
			baseMana:          150,
			modMana:           50,
			currentMana:       200,
			baseEnergy:        100,
			modEnergy:         10,
			currentEnergy:     110,
			position:          POS_STANDING,
		},
	}

	obj := &Object{
		id:                 0,
		category:           ITEM_WEAPON,
		wear:               ITEM_WEAR_MAIN_HAND,
		affected:           0x00,
		name:               "The sword of Daeldo",
		description:        "This beautiful hand-forged sword has a shiny blade and a very detailed handle that looks like a... HAHAHAHAHHAA",
		short_description:  []string{"sword", "daeldo"},
		action_description: "You swing around the sword of daeldo",
	}
	obj1 := &Object{
		id:                 1,
		category:           ITEM_ARMOR,
		wear:               ITEM_WEAR_FINGER,
		affected:           0x00,
		name:               "The ring of Foo",
		description:        "It's a ring with the inscription 'Foo' written on it",
		short_description:  []string{"ring", "foo"},
		action_description: "You twiddle your ring around your finger.",
	}
	obj2 := &Object{
		id:                 2,
		category:           ITEM_ARMOR,
		wear:               ITEM_WEAR_FINGER,
		affected:           0x00,
		name:               "The ring of Bar",
		description:        "It's a ring with the inscription 'Bar' written on it",
		short_description:  []string{"ring", "bar"},
		action_description: "You twiddle your ring around your finger",
	}

	rooms[0].addCharacter(&npc.Character)
	rooms[0].addObject(obj)
	rooms[0].addObject(obj1)
	rooms[0].addObject(obj2)

	return rooms
}
