package pkg

import (
	"crypto/rand"
	"log"
	"math/big"
	"reflect"
	"unicode"
)

/**
 * Stuff that should probably exist in Go (in my opinion)
 */

// Bitflag represents every flag used in the game
type Bitflag uint32

const NOFLAG = ^Bitflag(0)

// HasFlag returns TRUE if flag 'f' is present in 'flags'
func (f Bitflag) HasFlag(flags Bitflag) bool { return f&flags != 0 }

// AddFlag adds the flag to flags
func (f *Bitflag) AddFlag(flags Bitflag) { *f |= flags }

// ClearFlag removes the flag from flags
func (f *Bitflag) ClearFlag(flags Bitflag) { *f &= ^flags }

// ToggleFlag switches the state of given flag
func (f *Bitflag) ToggleFlag(flags Bitflag) { *f ^= flags }

/*
 * ContainsString returns true if string 'e' is contained in string 's'
 */
func ContainsString(s []string, e string) bool {
	for _, a := range s {
		if a == e {
			return true
		}
	}
	return false
}

/*
 * Min returns the lowest value between x and y
 */
func Min(x, y int) int {
	if x < y {
		return x
	}
	return y
}

/*
 * RandomInt returns a random integer
 */
func RandomInt(max int64) int64 {
	nBig, err := rand.Int(rand.Reader, big.NewInt(max))
	if err != nil {
		log.Println(err)
	}
	return nBig.Int64()
}

/*
 * Captalize returns the same string but With The First Letter Captalized
 */
func CaptalizeFirst(str string) string {
	for _, v := range str {
		u := string(unicode.ToUpper(v))
		return u + str[len(u):]
	}
	return ""
}

// Checks if an element exists in a slice
func ListExists(slice interface{}, item interface{}) bool {
	s := reflect.ValueOf(slice)

	if s.Kind() != reflect.Slice {
		panic("SliceExists() given a non-slice type")
	}

	for i := 0; i < s.Len(); i++ {
		if s.Index(i).Interface() == item {
			return true
		}
	}

	return false
}

func removeCharacterFromList(list []*Character, char *Character) ([]*Character, bool) {
	var aux []*Character
	for i := 0; i < len(list); i++ {
		if list[i] == char {
			aux = append(list[:i], list[i+1:]...)
			return aux, true
		}
	}
	return list, false
}

func addCharacterToList(list []*Character, char *Character) []*Character {
	var aux []*Character
	aux = append(list, char)
	return aux
}

func isCharacterInList(list []*Character, char *Character) bool {
	for i := 0; i < len(list); i++ {
		if list[i] == char {
			return true
		}
	}
	return false
}
