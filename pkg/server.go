package pkg

import (
	"io/ioutil"
	"net"
	"os"
	"strings"
	"time"

	"github.com/sirupsen/logrus"
)

var pulse uint64

type Server struct {
	connectionList []*Connection
	ticker         *time.Ticker
	Motd           string
	Menu           string
}

type World struct {
	roomList   map[uint32]*Room
	charList   []*Connection
	combatList []*Character
}

var DAHWORUDO *World

var ServerInstance *Server

func GetServer() *Server {

	if ServerInstance == nil {
		ServerInstance = &Server{
			connectionList: make([]*Connection, 0),
		}
		DAHWORUDO = &World{
			charList:   make([]*Connection, 0),
			combatList: make([]*Character, 0),
		}

		pwd, _ := os.Getwd()

		logrus.Info("[CONFIG] Current working directory set to " + pwd + ".\n")

		// 1. Pull in the welcome screen
		logrus.Info("[CONFIG] Pulling MOTD")
		motdBytes, _ := ioutil.ReadFile(pwd + "/resources/MOTD")
		ServerInstance.Motd = string(motdBytes)

		// 2. Pull in the menu
		logrus.Info("[CONFIG] Pulling Menu")
		menuBytes, _ := ioutil.ReadFile(pwd + "/resources/Menu")
		ServerInstance.Menu = string(menuBytes)

		// 3. Prepare the command hashes
		logrus.Info("[CONFIG] Preparing commands")
		prepareCommands()

		// 4. Load databases
		// Creating dummy space for navigation
		logrus.Info("[CONFIG] Loading rooms")
		DAHWORUDO.roomList = loadRooms()
	}

	return ServerInstance
}

func (server *Server) AddConnection(connection net.Conn) *Connection {
	newConnection := Connection{conn: connection, timeConnected: time.Now(), state: STATE_WELCOME}
	server.connectionList = append(server.connectionList, &newConnection)
	go newConnection.listen()
	logrus.Info("[CONN] There are", server.ConnectionCount(), "connected users.\n")
	return &newConnection
}

func (server *Server) onClientConnectionClosed(connection *Connection, err error) {

	for i, conn := range server.connectionList {
		if conn == connection {
			server.connectionList[i] = server.connectionList[len(server.connectionList)-1]
			server.connectionList[len(server.connectionList)-1] = nil
			server.connectionList = server.connectionList[:len(server.connectionList)-1]
			break
		}
	}

	logrus.Info("[DISC] There are", server.ConnectionCount(), "connected users.\n")
}

/**
 * User log-in process
 */
func (server *Server) login(username string, password string) (bool, *Character) {

	if !userExists(username) {
		return false, nil
	}

	char := authenticate(username, password)

	if char != nil {
		return true, char
	}

	return true, nil
}

/**
 * Check the database to see if the char exists
 * TODO perform an actual database scan
 */
func userExists(username string) bool {
	return username == "Zaphiran" || username == "Lucas"
}

/**
 * Authenticate a user via database, and fetch the char
 * TODO implement the actual check!
 */
func authenticate(username string, password string) *Character {
	if username == "Zaphiran" && password == "123" ||
		username == "Lucas" && password == "123" {
		char := &Character{
			Name:              username,
			short_description: []string{username},
			CurrentRoom:       DAHWORUDO.GetRoom(0),
			baseHit:           100,
			modHit:            50,
			currentHit:        150,
			baseMana:          150,
			modMana:           50,
			currentMana:       200,
			baseEnergy:        100,
			modEnergy:         10,
			currentEnergy:     110,
			position:          POS_STANDING,
		}
		return char
	}
	return nil
}

/**
 * Server-side trigger when authentication occurs in the comm handler
 */
func (server *Server) onCharacterAuthenticated(connection *Connection) {
	logrus.Info("[AUTH] Character authenticated " + connection.char.Name + ".\n")
	DAHWORUDO.charList = append(DAHWORUDO.charList, connection)

	connection.state = STATE_LOGIN_MENU
	connection.Write("Welcome. Death Awaits.\n")
	connection.sendMenu()
}

/**
 * Start the main game loop
 * The Main Loop.  The Big Cheese.  The Top Dog.  The Head Honcho.  The..
 */
func (server *Server) Start() {
	server.ticker = time.NewTicker(time.Millisecond * 100)
	pulse = 0
	go func() {
		for range server.ticker.C {
			for _, connection := range DAHWORUDO.charList {
				if connection.char.HasOutput() {
					connection.char.SendPrompt()
					connection.Write(connection.char.GetOutput())
					connection.char.ClearOutput()
				}
				if connection.char != nil {
					//TODO character processing and event treatments, as well as heartbeat
					//Subtraction of WaitState
					//Pulls character out of void

					//Checks what the char is doing (reading prompts etc)
					/////if he is not doing special things, normal command execution
					/////checks if he is using an alias, else calls command interpreter

					//Sends output to chars
					//Sends prompt to chars
					//Kick out people who are disconnected
				}
			}
			//Is it really needed to check for missed pulses?
			heartbeat(pulse)
		}
	}()

}

/**
 * Executes scripts according to need
 */
func heartbeat(pulse uint64) {

	//event_process -> dg_events (called every heartbeat)

	//script_trigger_check -> dg_scripts (called every 13 seconds World triggers)

	//zone_updates -> db? (called every 10 seconds)

	//check_idle_passwords (called every 15 seconds)
	//kicks people out if no password given
	//(I think this can be implemented in the login function)

	//mobile_activity -> mobact (called every 10 seconds)

	performViolence()

	//runs a mud hour (75 seconds)
	////weather_and_time (increases hour and changes the weather)
	////check_time_triggers (check time triggers -> script_trigger_check Time triggers)
	////affect_update (updates char affects for every char)
	////point_update (updates statuses of pc's npc's and objects in the game)
	////check_timed_quests (Timed quest triggers)

	//auto save configuration (minutes are read from config file)
	////saves all houses and chars

	//record_usage (5 minutes) (checks the number of connected / playing sockets and logs it)

	//save_mud_time (saves the current mud hour / date to load on world creation)

	//extract characters that are away for too long / disconnected

}

/**
 * How many connections are active?
 */
func (server *Server) ConnectionCount() int {
	return len(server.connectionList)
}

/**
 * A message has been received on a given connection descriptor
 */
func (server *Server) onMessageReceived(connection *Connection, message string) {

	if len(message) == 0 {
		connection.char.SendPrompt()
		return
	}
	words := strings.Fields(message)
	input, arguments := words[0], words[1:]

	logrus.Trace(input, arguments)

	connection.char.do(input, arguments)
}

func (server *Server) GetRoom(roomId uint32) (room *Room) {
	room = DAHWORUDO.roomList[roomId]
	return room
}
