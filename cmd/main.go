package main

import (
	"net"
	"os"

	"github.com/sirupsen/logrus"
	"gitlab.com/adkairalla/i4-golang/pkg"
)

func main() {
	logrus.SetLevel(logrus.TraceLevel)
	service := "127.0.0.1:7777"

	tcpAddr, err := net.ResolveTCPAddr("tcp4", service)
	logrus.Info("TcpAddress: ", tcpAddr)
	checkError(err)
	listener, err := net.ListenTCP("tcp", tcpAddr)
	checkError(err)
	defer listener.Close()

	pkg.GetServer().Start()
	listenForConnections(listener)
}

func listenForConnections(listener *net.TCPListener) {
	for {
		conn, err := listener.Accept()
		if err != nil {
			continue
		}
		newDescriptor(conn)
	}
}

func checkError(err error) {
	if err != nil {
		logrus.Fatal("Fatal Error: ", err.Error())
		os.Exit(1)
	}
}

func newDescriptor(connection net.Conn) {
	pkg.ServerInstance.AddConnection(connection)
}
